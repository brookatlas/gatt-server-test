package com.example.gattserver

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelUuid
import android.util.Log
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {


    private var gattServer: BluetoothGattServer? = null
    private lateinit var btManager: BluetoothManager
    private val REQUEST_ENABLE_BT: Int = 5
    private val LOGTAG = "gattServerActivity"
    private val registeredDevices = mutableSetOf<BluetoothDevice>()

    /*
     * function name: onCreate
     * called when the activity is created
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // map the startButton to this variable
        var startButton:Button = findViewById(R.id.startGattServer) as Button
        // keeps the screen always on
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        startButton.setOnClickListener {
            // 1. check that ble is supported
            if(isBluetoothLESupported())
            {
                // 2. create intent filter, forward bluetooth events to btReceiver
                val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
                registerReceiver(btReceiver, filter)
                // 3. enable bluetooth if disabled
                enableBluetoothIfDisabled()
                val adapter = btManager.adapter
                if(!adapter.isEnabled)
                {
                    Log.d(LOGTAG, "bluetooth is disabled.. enabling")
                    adapter.enable()
                }
                else
                {
                    Log.d(LOGTAG, "bluetooth enabled.. starting the gatt server")
                    startAdvertising()
                    startServer()
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    /*
     * function name: onDestroy
     * 1. stops the server
     * 2. stops the advertising
     */
    override fun onDestroy() {
        super.onDestroy()
        val adapter = btManager.adapter
        if(adapter.isEnabled)
        {
            stopServer()
            stopAdvertising()
        }
    }

    /*
     * function name: isBluetoothLESupported
     * description:
     * checks if bluetooth LE is supported on the device
     * if supported - returns true
     * else - returns false, and finishes the activity(exits app)
     */
    private fun isBluetoothLESupported(): Boolean {
        val hasBLE: Boolean = packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
        if(hasBLE)
        {
            Toast.makeText(this, "BLE supported, yay!", Toast.LENGTH_SHORT).show()
            return true;
        }
        else
        {
            Toast.makeText(this, "BLE not supported here", Toast.LENGTH_SHORT).show()
            finish()
            return false;
        }
    }

    // initializing the bluetooth adapter
    private val btAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        btManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        btManager.adapter
    }

    // enables the bluetooth on the device if disabled
    private fun enableBluetoothIfDisabled() {
        btAdapter?.takeIf { !it.isEnabled }?.apply {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        return
    }

    // a broadcast reciever(a.k.a event bus, listens to events of certain type)
    // listens to bluetooth events only
    private val btReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
            when(state)
            {
                BluetoothAdapter.STATE_ON -> {
                    // start advertising
                    // start server
                }
                BluetoothAdapter.STATE_OFF -> {
                    // stop server
                    // stop advertising
                }
            }
        }
    }

    /*
     * function name: startServer()
     * starts the gatt server.
     */
    private fun startServer(){
        gattServer = btManager.openGattServer(this, gattServerCallback)
        gattServer?.addService(tempProfile.createTempService())
                ?: Log.w(LOGTAG, "unable to create GATT server")
    }

    /*
     * function name: startServer()
     * stops the gatt server.
     */
    private fun stopServer(){
        gattServer?.close()
    }

    /*
     * function name: startAdvertising
     * starts the advertising of the LE service
     */
    private fun startAdvertising(){
        val btAdvertiser: BluetoothLeAdvertiser? =
                btManager.adapter.bluetoothLeAdvertiser
        btAdvertiser?.let {
            val settings = AdvertiseSettings.Builder()
                    .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                    .setConnectable(true)
                    .setTimeout(0)
                    .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                    .build()
            val data = AdvertiseData.Builder()
                    .setIncludeDeviceName(true)
                    .setIncludeTxPowerLevel(false)
                    .addServiceUuid(ParcelUuid(tempProfile.TEMP_SERVICE))
                    .build()
            it.startAdvertising(settings, data, advertiseCallback)

        }?: Log.w(LOGTAG, "failed to start advertiser!")
    }

    /*
     * function name: startAdvertising
     * stops the advertising of the LE service
     */
    private fun stopAdvertising(){
        val btAdvertiser: BluetoothLeAdvertiser? =
                btManager.adapter.bluetoothLeAdvertiser
        btAdvertiser?.let {
            it.stopAdvertising(advertiseCallback)
        } ?: Log.w(LOGTAG, "Failed to create advertiser!")
    }

    /*
     * callback function for advertisement.
     * logs a message when failed to advertise.
     * logs a message when it succeeds to advertise as well.
     */
    private val advertiseCallback = object : AdvertiseCallback(){
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            Log.i(LOGTAG, "LE advertising started successfully")
        }

        override fun onStartFailure(errorCode: Int) {
            Log.w(LOGTAG, "LE Advertising failed: $errorCode")
            finish()
        }
    }


    /*
     * gatt server callback's here.
     * onConnectionStateChange - called when a new device connects to the gatt server.
     * onCharacteristicReadRequest - called when a connected device attempts to read a characteristic from the gatt server.
     */
    private val gattServerCallback = object: BluetoothGattServerCallback(){
        override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
            // checks if the state is connected or disconnected.
            // logs the event with the name of the device who connects/disconnects.
            if(newState == BluetoothProfile.STATE_CONNECTED)
            {
                Log.i(LOGTAG, "a device named: $device has connected")
            }
            else if(newState == BluetoothProfile.STATE_DISCONNECTED)
            {
                Log.i(LOGTAG, "the device named: $device has disconnected")
                registeredDevices.remove(device)
            }
        }


        override fun onCharacteristicReadRequest(device: BluetoothDevice?, requestId: Int, offset: Int, characteristic: BluetoothGattCharacteristic?) {
            when{
                // checks the uuid of the requested characteristic.
                // if matches any of the available characteristics, the matching characteristic data is returned.
                characteristic?.uuid == tempProfile.CURRENT_TEMP -> {
                    Log.i(LOGTAG, "Read current temperature")
                    gattServer?.sendResponse(
                            device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            tempProfile.getTemp()
                    )
                }
                characteristic?.uuid == tempProfile.CURRENT_PRESSURE -> {
                    Log.i(LOGTAG, "Read current pressure")
                    gattServer?.sendResponse(
                            device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            tempProfile.getPressure()
                    )
                }
            }
        }
    }



}
