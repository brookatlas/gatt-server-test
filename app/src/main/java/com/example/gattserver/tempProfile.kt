package com.example.gattserver

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.Context
import android.hardware.SensorManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.getSystemServiceName
import java.util.*

/*
 * a temperature profile object.
 * represents a gatt profile.
 * this profile has two characteristics:
 * 1. for temperature
 * 2. for pressure
 * all of the data in those characteristics is fake and constant.
 * note:
 *  each byte array returned through characteristics, will always return the last two bytes as 0x0
 */
object tempProfile {

    /* temperature service random UUID */
    val TEMP_SERVICE: UUID = UUID.fromString("d23db7f0-8cfc-480b-be45-ef151f239657")
    /* current temperature charateristic random UUID */
    val CURRENT_TEMP: UUID = UUID.fromString("6c366661-0ceb-4799-823b-f158ec1d0de5")
    /* current pressure characteristic random UUID */
    val CURRENT_PRESSURE: UUID = UUID.fromString("b2e1cc6e-e0fb-47ab-a9b8-0d87fae86db5")

    // creates the temperature gatt service
    fun createTempService(): BluetoothGattService {
        val service = BluetoothGattService(
            TEMP_SERVICE, BluetoothGattService.SERVICE_TYPE_PRIMARY
        )
        val currentTemperature = BluetoothGattCharacteristic(
            CURRENT_TEMP,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            BluetoothGattCharacteristic.PERMISSION_READ
        )
        val currentPressure = BluetoothGattCharacteristic(
            CURRENT_PRESSURE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            BluetoothGattCharacteristic.PERMISSION_READ
        )
        service.addCharacteristic(currentTemperature)
        service.addCharacteristic(currentPressure)
        return service
    }

    /*
        *
        * get the current device's temperature
        *
     */
    fun getTemp(): ByteArray {
        val field = ByteArray(3)
        field[0] = (5.4).toByte()
        field[1] = 0x0
        field[2] = 0x0
        return field
    }

    /*
        *
        * get the device's current pressure
        *
     */
    fun getPressure(): ByteArray {
        val field = ByteArray(3)
        field[0] = (3.3).toByte()
        field[1] = 0x0
        field[2] = 0x0
        return field
    }

}